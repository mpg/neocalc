# NeoCalc

Stack based Calculator.

Multiple Stacks, copy or reference from  one to another.
Save Stacks for latter use.

Partial application of functions.

Easy to manipulate computation.

Mathematical and Physical constants.

Define and use functions.

Apply calculations to a number of quntatyes simeltaniosly.


# State of project

After a few iterations I'v setteld on the arcutecutre.
Basic stack calculator working.
A few constants in there.
Next step is to work on collectons.

# Structure and Philosophy 

## The Stack

The stack is the way working memory is modeled.
ideal interactions:

```
> 1 3 3 + +
(+ 1 (+ 3 3)) | 7
```

```
> pi 3 / sin
(sin (/ pi 3)) | 0.86603
````

```
> sin pi 3 / 
(sin .) (/ pi 3) | 1.0472
> <
(sin (/ pi 3) | 0.886603
> >
(sin .) (/ pi 3) | 1.0472 
> >
(sin .) (/ pi 3 ) 3 | 3
> > 
(sin .) (/ . .) pi 3 | 3
> <<
(sin (/ . .)) pi 3 | 3
> >> 
(sin .) (/ . .) pi 3 | 3
> <<<
(sin (/ pi 3) | 0.886603
```

```
> cos sin > /
(/ sin(.) cos(.))
> $1 < $1 < 
(/ (sin $1) (cos $1))
> pi 3 / <
(/ (sin (/ pi 3)) (cos (/ pi 3)))
> >
(/ (sin $1) (cos $1)) (/ pi 3)
> rm
(/ (sin $1) (cos $1))
> !Tan
(Tan .)
> rm
> pi 3 / Tan
(Tan pi 3)
```

## Stackobjets

Every object on the stack  is a subclass of a StackObject.

StackObjects are trees that represent computations.

StackObjects children are stored in the .ocupation method.

The number of children a particular StackObject exspectes is stored in 
the .valance atribulte.

To add a chiled to a StackObject, call it like a function, parsing the new chield.

StackObjects have .value mehtod that return a value.

if the valnce is the ocupation then .value calles .compute
which is left to the subclasses of StackObject.

Numbers .compute return its self.

if valance is less then the ocupation the .value return she stack object its self.
when the S
