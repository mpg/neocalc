#!/bin/env python3
import math
from cmd import Cmd
from tabulate import tabulate as tab
import sys
from pathlib import Path

class StackObject:
    valence = 0 # How many values dose this object need to return
    ocupation = [] # How many values is it supplied with
    init_vals = () # What values where passed to __init__
    sym = '&'

    def left(self):
        me = self.valence - len(self.ocupation)
        return me + sum(map(lambda obj: obj.left(), self.ocupation))

    def more(self):
        return self.left() > 0

    def value(self):
        ocupied = len(self.ocupation)
        if ocupied == self.valence:
            return self.compute()
        else:
            return self

    def new(self):
        return type(self)(*self.init_vals)

    # interupted hear
    def __call__(self,obj):
        new = self.new()
        new.ocupation = [*self.ocupation]
        if len(self.ocupation) < self.valence:
            new.ocupation.append(obj)
        else:
            for i in range(len(new.ocupation)):
                if new.ocupation[i].more():
                    new.ocupation[i] = new.ocupation[i](obj)
                    break
        return new

    def __str__(self):
        return 'unnamed Stack Object'

    def __repr__(self):
        return str(self)

class Operation(StackObject):
    def __init__(self,sym,op):
        self.init_vals = (sym,op)
        self.sym = sym
        self.op = op

    def compute(self):
        ocupied_values = [item.value() for item in self.ocupation]
        if self.more():
            return self
        return Number(self.op(*[float(value) for value in ocupied_values]))

    def __str__(self):
        l = ' '.join([self.sym,
                *[str(item) for item in self.ocupation],
                *['.' for _ in range(self.valence - len(self.ocupation))]])
        return f'({l})'

class Number(StackObject):
    def __init__(self,val,name=None):
        self.init_vals = (val)
        val = float(val)
        self.val = val
        self.name = name
        self.sym = str(self)

    def compute(self):
        return self

    def __str__(self):
        if self.name:
            return self.name
        n = round(self.val,5)
        if n.is_integer(): n = int(n)
        return str(n)

    def __float__(self):
        return self.val

    def new(self):
        return Number(self.val)

class Transform(Operation):
    valence = 1

class BiOp(Operation):
    valence = 2

consts = {
        'pi': Number(math.pi,name='pi'),
        'e' : Number(math.e,name='e'),
        }

ops = { op.sym:op for op in [
    BiOp('+',lambda x,y:x+y),
    BiOp('-',lambda x,y:x-y),
    BiOp('*',lambda x,y:x*y),
    BiOp('/',lambda x,y:x/y),
    BiOp('**',lambda x,y:x**y),
    BiOp('log',lambda x,y: math.log(x,y)),
    Transform('exp',lambda x: math.e ** x),
    Transform('ln',math.log),
    Transform('sqrt',math.sqrt),
    Transform('sin',math.sin),
    Transform('cos',math.cos),
    Transform('tan',math.tan),
    Transform('asin',math.asin),
    Transform('acos',math.acos),
    Transform('atan',math.atan),
    ]}

############# CLI ##################

def try_(foo):
    def try_foo(s):
        try:
            return foo(s)
        except:
            return False
    return try_foo

try_int = try_(int)

def listStack(stack):
    return tab([
        ['#','RES','CLC'],# headers
        *[[len(stack) - i - 1 ,obj.value(),str(obj)]
        for i,obj in enumerate(stack)]],
        numalign='left',
        headers='firstrow',
        tablefmt='simple',
        )

def roll(list,N):
    if N == 0: return list
    N = N % len(list)
    for _ in range(N):
        list.insert(0,list.pop())
    return list

def apply(stack,obj):
    valence = obj.left()
    l = len(stack)
    from_end = l-valence if valence < l else 0
    objs = stack[from_end:]
    stack = stack[:from_end]
    for o in objs:
        obj = obj(o)
    stack.append(obj)
    return stack

mkprompt = lambda s: s + ' > '
class Cli(Cmd):
    oldroll = 0

    stackname = 'main'
    stack,future = [],[]
    stacks = {stackname:(stack,future)}
    prompt = mkprompt(stackname)

    fileroot = Path.home().joinpath('.neocalc')
    if not fileroot.exists():
        fileroot.mkdir()
        print('creating .neocalc file')
    stackfiles = fileroot.joinpath('stacks')
    if not stackfiles.exists():
        stackfiles.mkdir()

    def _stackname(self,text,line,begid,endidx):
        'stack name compleation'
        return [stack for stack in self.stacks if stack.startswith(text)]

    complete_stack = _stackname
    def do_stack(self,line):
        if line == self.stackname:
            return False

        self.stacks[self.stackname] = (self.stack,self.future)
        if line in self.stacks:
            self.stack,self.future = self.stacks[line]
        else:
            self.stack,self.future = [],[]
            self.stacks.update({line:(self.stack,self.future)})

        self.stackname = line
        self.prompt = mkprompt(line)


    def default(self,line):
        if ' ' in line:
            for word in line.split(' '):
                self.onecmd(word)
            return False

        if line in ops:
            obj = ops[line]

        elif line in consts:
            obj = consts[line]

        elif line == '<':
            if len(self.stack) < 2:
                return False
            a = self.stack.pop()
            b = self.stack.pop()
            if b.more():
                self.stack.append(a)
                obj = b
            else:
                self.stack.append(b)
                self.stack.append(a)
                return False

        elif line == '>':
            if len(self.stack) < 1:
                return False
            obj = self.stack.pop()
            if len(obj.ocupation) < 1:
                self.stack.append(obj)
                return False
            chi = obj.ocupation.pop()
            self.stack.append(obj)
            self.stack.append(chi)
            return False
         
        else:
            try:
                obj = Number(line)
            except:
                print('unknown symbol')
                return False

        self.stack = apply(self.stack,obj)

    def do_swap(self,line):
        if len(self.stack) >= 2:
            a = self.stack.pop()
            b = self.stack.pop()
            self.stack.append(a)
            self.stack.append(b)


    def do_rm(self,line):
        if len(self.stack) > 0:
            print('removed ',self.stack.pop())

    def do_un(self,line):
        if len(self.stack) > 0:
            obj = self.stack.pop()
            self.future.append(obj.new())
            print('undoing ',obj.sym)
            for chield in obj.ocupation:
                self.stack.append(chield)

    def do_re(self,line):
        if len(self.future) > 0: 
            obj = self.future.pop()
            self.stack = apply(self.stack,obj)
            

    complete_ls = _stackname
    def do_ls(self,line):
        self.do_listStack(line)

    def do_roll(self,line):
        n = try_int(line) or 1
        self.stack = roll(self.stack,n)
        self.oldroll = n

    def do_unroll(self,line):
        n = -self.oldroll
        self.stack = roll(self.stack,n)
        self.oldroll = n

    def precmd(self,line):
        self.print_post = True
        return line

    def postcmd(self,stop,line):
        if len(self.stack) == 0 or not self.print_post:
            return stop

        for obj in self.stack:
            print(obj,end=' ')
        print(' | ',obj.value())

        return stop

    def emptyline(self):
        pass

    complete_dumpStack = _stackname
    def do_dumpStack(self,stackname):
        stackname = stackname if stackname != '' else self.stackname
        stackpath = self.stackfiles.joinpath(stackname)

        if stackname not in self.stacks:
            print('un-known stack')
            return False

        if stackpath.exists():
            print('stack file exists, overwriting')

        if stackname == self.stackname:
            self.stacks[stackname] = (self.stack,self.future)

        print('writing to',stackpath)
        with open(stackpath,'w') as stackfile:
            stackfile.write(listStack(self.stacks[stackname][0]))
            stackfile.write('\n')

    def do_loadStack(self,stackname):
        print('not implemented')

    complete_listStack = _stackname
    def do_listStack(self,line):
        self.print_post = False
        if line in self.stacks and line != self.stackname:
            stack = self.stacks[line][0]
        else:
            stack = self.stack
        print(listStack(stack))

    def do_listFuture(self,line):
        for obj in self.future:
            print(obj)

    def do_listConstants(self,line):
        for key in consts:
            print(key,consts[key].val)

    def do_EOF(self,line):
        return True


if __name__ == '__main__':
    cli = Cli()
    args = ' '.join(sys.argv[1:])
    line = None
    if '//' in args:
        for line in args.split('//'):
            cli.onecmd(line)
    if line == '':
        cli.onecmd('ls')
    else:
        try:
            cli.cmdloop()
        except KeyboardInterrupt:
            pass
        stack = cli.stack
        if len(stack) != 0:
            obj = stack[-1]
